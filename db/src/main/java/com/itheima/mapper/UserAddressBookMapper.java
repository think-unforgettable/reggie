package com.itheima.mapper;

import com.itheima.domain.AddressBook;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/13    20:36
 */
@Mapper
public interface UserAddressBookMapper {

    /**
     * 添加地址的方法
     * @param addressBook
     * @return
     */
    int insertAddress(AddressBook addressBook);

    /**
     * 获取地址列表
     * @return
     */
    List<AddressBook> selectAddress(@Param("uid") Long uid );


    /**
     * 设置默认地址为 0
     * @param uid
     * @return
     */
    int updateDefaults(@Param("uid") Long uid);
    //再设置id相对应的地址为默认地址

    /**
     * 设置默认地址为 1
     * @param id
     * @return
     */
    int updateDefault(@Param("id") Long id,@Param("uid") Long uid);

    /**
     * 查询uid下的默认地址
     * @param uid
     * @return
     */
    AddressBook selectAddressDefault(@Param("uid") Long uid);

    /**
     * 返回修改地址
     * @param id
     * @param uid
     * @return
     */
    AddressBook updatelist(@Param("id") Long id ,@Param("uid") Long uid);

    /**
     * 修改地址
     * @param addressBook
     * @return
     */
    int updateAddress(AddressBook addressBook);

    /**
     *
     * @param uid
     * @param id
     * @return
     */
    int deleteAddress(@Param("uid") Long uid,@Param("id") Long id);


}
