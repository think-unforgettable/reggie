package com.itheima.mapper;

import com.itheima.domain.Dish;
import com.itheima.dto.DishDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    16:22
 */
@Mapper
public interface UserDishMapper {


    /**
     * 前端分类中的菜品
     * @param categoryId
     * @param status
     * @return
     */
    List<Dish> listDish(@Param("categoryId") Long categoryId, @Param("status") int status);
}
