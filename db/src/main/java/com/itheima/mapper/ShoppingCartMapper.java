package com.itheima.mapper;

import com.itheima.domain.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author CSY
 * 2022/5/12    15:42
 */
@Mapper
public interface ShoppingCartMapper {


    /**
     * 购物车展示
     * @return
     */
    List<ShoppingCart> shoppingCarts(@Param("uid") Long uid);





    //购物车添加相关sql
    int shoppingCartAdd(ShoppingCart shoppingCart);//添加
    int getdishByUidDishid(ShoppingCart shoppingCart);//查询总份数
    int updateNumber(ShoppingCart shoppingCart);//修改点餐数量
    List<ShoppingCart> selectNumber(ShoppingCart shoppingCart);//动态获取number
    //购物车添加相关sql



    /**
     * 查询uid相关购物车数据
     * @param userId
     * @return
     */
    List<ShoppingCart> selectShoppCartByUserId(@Param("userId") Long userId);


    /**
     * 清空购物车
     * @param uid
     * @return
     */
    int delect(@Param("uid") Long uid);

    /**
     * 相关用户的购物车的总数
     * @param uid
     * @return
     */
    int count(@Param("uid") Long uid);




    /**
     * 删减份数
     * @param shoppingCart
     * @return
     */
    int sub(ShoppingCart shoppingCart);

    int subOne(ShoppingCart shoppingCart);

    /**
     * 获取购物车的总价格
     * @param id
     * @return
     */
    BigDecimal getCountAmount(@Param("id") Long id);


}
