package com.itheima.mapper;

import cn.hutool.core.date.DateTime;
import cn.hutool.db.sql.Order;
import com.itheima.domain.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/11    15:15
 */
@Mapper
public interface OrdersMapper {

    List<Orders> page(@Param("page") int page,
                      @Param("pageSize")int pageSize,
                      @Param("number") String number,
                      @Param("beginTimes") String begintimes ,
                      @Param("endTimes") String endtimes );

    /**
     * 查总数
     * @return
     */
    int count(@Param("number") String number);

    /**
     * 修改状态
     * @param orders
     * @return
     */
    int updateStatus(Orders orders);
}
