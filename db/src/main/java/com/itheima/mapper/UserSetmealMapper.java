package com.itheima.mapper;

import com.itheima.domain.Setmeal;
import com.itheima.domain.SetmealDish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    18:05
 */
@Mapper
public interface UserSetmealMapper {


    List<Setmeal> selectSetmeal(@Param("categoryId") Long categoryId,
                                @Param("status")int status);



    List<SetmealDish> getSetmealDish(@Param("setmealId")Long setmealId);
}
