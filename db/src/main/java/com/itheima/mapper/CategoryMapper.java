package com.itheima.mapper;

import com.itheima.domain.Category;
import com.itheima.vo.CategoryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    19:08
 */
@Mapper
public interface CategoryMapper {

    /**
     * 分类管理分页查询
     * @param page
     * @param pageSize
     * @return
     */
    List<Category> selectpage(@Param("page") Integer page, @Param("pageSize") Integer pageSize);

    /**
     * 查询总数
     * @return
     */
    int selectCount();

    /**
     * 删除分类的方法
     * @return
     */
    int delete(@Param("id") Long id);

    /**
     * 新增的方法
     * @param category
     * @return
     */
    int insert(Category category);

    /**
     * 根据id修改
     * @param category
     * @return
     */
    int updateById(Category category);

    /**
     * 通过id查分类名称
     * @param id
     * @return
     */
    String selectById(@Param("id") Long id);

    /**
     * 通过类型获取
     * @param type
     * @return
     */
    List<Category> seleteByType(@Param("type") int type);

//=====================================================================


    List<Category> selectAll();

}
