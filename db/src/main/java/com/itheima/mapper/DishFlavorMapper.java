package com.itheima.mapper;

import com.itheima.domain.DishFlavor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    21:47
 */
@Mapper
public interface DishFlavorMapper {

//    /**
//     *
//     * @return
//     */
//    List<DishFlavor> selectAll();

    /**
     * 添加的菜品喜好
     * @param dishFlavor
     * @return
     */
     int insert(DishFlavor dishFlavor);

    /**
     * 查单条
     * @param id
     * @return
     */
     List<DishFlavor> byDishId(@Param("id") Long id);

    /**
     * 修改菜品对应的喜好
     * @param dishFlavor
     * @return
     */
     int update(DishFlavor dishFlavor);

}
