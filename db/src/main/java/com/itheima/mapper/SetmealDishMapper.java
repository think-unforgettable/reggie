package com.itheima.mapper;

import com.itheima.domain.Setmeal;
import com.itheima.domain.SetmealDish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/10    21:50
 */
@Mapper
public interface SetmealDishMapper {

    /**
     * 通过service  for循环获取每个SetmealDish
     * @return
     */
    int insert(SetmealDish setmealDish);

    /**
     * selectSetmealDishBySetmealId
     * @param id
     * @return
     */
    List<SetmealDish> selectSetmealDishBySetmealId(@Param("id") Long id);

    /**
     * 根据id删除套餐中的菜品
     * @param id
     * @return
     */
    int deleteById(@Param("id") Long id);

}
