package com.itheima.mapper;

import com.itheima.domain.AddressBook;
import com.itheima.domain.OrderDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/14    16:01
 */
@Mapper
public interface UserOrderDetailMapper {

    /**
     * 添加订单信息
     * @param orderDetail
     * @return
     */
    int addOrderDetail(OrderDetail orderDetail);

    /**
     * 根据订单id获取订单详情
     * @param oid
     * @return
     */
    List<OrderDetail> orderDetails(@Param("oid") Long oid);

    /**
     *
     * @param addressId
     * @return
     */
    AddressBook getAddressInfo(@Param("addressId") Long addressId);




}
