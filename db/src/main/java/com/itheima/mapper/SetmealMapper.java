package com.itheima.mapper;

import com.itheima.domain.Setmeal;
import com.itheima.dto.SetmealDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/10    21:09
 */
@Mapper
public interface SetmealMapper {
    /**
     * 分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    List<Setmeal> page(@Param("page") int page,
                       @Param("pageSize") int pageSize,
                       @Param("name") String name);

    /**
     * 查总数
     * @return
     */
    int selectAll(@Param("name") String name);

    /**
     * 按ids删除
     * @param ids
     * @return
     */
    int deleteByIds(@Param("ids") List<Long> ids);

    /**
     * 修改状态
     * @param uid
     * @param status
     * @param ids
     * @return
     */
    int updateStatus(@Param("uid") Long uid,@Param("status") int status,@Param("ids") List<Long> ids);

    /**
     * 添加套餐信息
     * @param setmealDto
     * @return
     */
    int insert(SetmealDto setmealDto);

    /**
     * 查单个套餐
     * @param id
     * @return
     */
    Setmeal selectSetmealById(@Param("id") Long id);

    /**
     * 根据id修改套餐
     * @param setmealDto
     * @return
     */
    int updateById(SetmealDto setmealDto);

}
