package com.itheima.mapper;

import com.itheima.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author CSY
 * 2022/5/12    11:45
 */
@Mapper
public interface UserMapper {

    /**
     * 查询用户存在否？不存在则创建
     * @param phone
     * @return
     */
    UserDto selectByPhone(@Param("phone") String phone);

    /**
     * 创建新用户
     * @param userDto
     * @return
     */
    int insertUser(UserDto userDto);


}
