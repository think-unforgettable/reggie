package com.itheima.mapper;

import com.itheima.domain.Dish;
import com.itheima.dto.DishDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    21:11
 */
@Mapper
public interface DishMapper {
    /**
     * 菜品分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    List<Dish> page(@Param("page") int page,
                       @Param("pageSize") int pageSize,
                       @Param("name") String name
                    );

    /**
     * 查询总数
     * @return
     */
    int selectCount(@Param("name") String name);

    /**
     * 修改状态
     * @param uid
     * @param status
     * @param ids
     * @return
     */
    int updateStatus(@Param("uid") Long uid,@Param("status") int status,@Param("ids") List<Long> ids);

    /**
     * 删除
     * @param ids
     * @return
     */
    int deleteByIds(@Param("ids") List<Long> ids);

    /**
     * 添加的方法
     * @param dish
     * @return
     */
    int insert(Dish dish);

    /**
     * name查询id
     * @param name
     * @return
     */
    Long selectIdByName(String name);

    /**
     * 查对象
     * @param id
     * @return
     */
    Dish byId(@Param("id") Long id);

    /**
     * 修改的方法
     * @param dish
     * @return
     */
    int update(Dish dish);

    /**
     * selectDishcategoryId
     * @param id
     * @return
     */
    List<Dish> selectDishcategoryId(Long id);

}
