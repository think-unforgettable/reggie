package com.itheima.mapper;

import com.itheima.domain.Category;
import com.itheima.domain.Employee;
import com.itheima.dto.EmpAddDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/7    20:38
 */
@Mapper
public interface EmployeeMapper {

    /**
     *          登录
     * @param username
     * @param password
     * @return
     */
    Employee selectByUP(@Param("username") String username,@Param("password") String password);

    List<Employee> selectPage(@Param("page") int page,
                              @Param("pageSize") int pageSize,
                              @Param("name") String name);

    /**
     * 查询总数的方法
     * @param name
     * @return
     */
    int selectCount(@Param("name") String name);

    /**
     * 添加的方法
     * @return
     */
    int inset(EmpAddDto empAddDto);

    /**
     *  修改员工信心
     * @param employee
     * @return
     */
    int update(Employee employee);

    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    Employee selectById(String id);


}
