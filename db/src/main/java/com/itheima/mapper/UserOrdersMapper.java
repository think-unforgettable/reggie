package com.itheima.mapper;

import com.itheima.domain.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/14    14:36
 */
@Mapper
public interface UserOrdersMapper {


    /**
     * 添加订单信息
     * @param orders
     * @return
     */
    int AddressOrders(Orders orders);

    /**
     * 通过用户id获取订单
     * @param uid
     * @return
     */
    List<Orders> orderses(@Param("uid") Long uid,
                          @Param("page") int page,
                          @Param("pageSize") int pageSize);

    /**
     * 根据用户id查询订单总数
     * @param uid
     * @return
     */
    int count(@Param("uid") Long uid);



}
