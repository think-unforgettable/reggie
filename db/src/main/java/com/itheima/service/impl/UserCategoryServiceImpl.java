package com.itheima.service.impl;

import com.itheima.domain.Category;
import com.itheima.mapper.CategoryMapper;
import com.itheima.service.UserCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    15:13
 */
@Service
public class UserCategoryServiceImpl implements UserCategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 查前台所有
     *
     * @return
     */
    @Override
    public List<Category> getAllCategory() {

        return categoryMapper.selectAll();
    }
}
