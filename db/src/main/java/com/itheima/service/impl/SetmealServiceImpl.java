package com.itheima.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import com.itheima.common.TokenHolder;
import com.itheima.domain.Setmeal;
import com.itheima.domain.SetmealDish;
import com.itheima.dto.SetmealDto;
import com.itheima.mapper.SetmealMapper;
import com.itheima.service.SetmealDishService;
import com.itheima.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author CSY
 * 2022/5/10    21:19
 */
@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private SetmealDishService setmealDishService;


    /**
     * 分页查询
     *
     * @param name
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public List<Setmeal> page(String name, int page, int pageSize) {
        page=(page-1)*pageSize;

        return setmealMapper.page(page,pageSize,name);
    }

    /**
     * 查总数
     *
     * @param name
     * @return
     */
    @Override
    public int selectAll(String name) {
        return setmealMapper.selectAll(name);
    }

    /**
     * 按ids删除
     *
     * @param ids
     * @return
     */
    @Override
    public boolean deleteByIds(List<Long> ids) {


        int i = setmealMapper.deleteByIds(ids);


        return i>0;
    }

    /**
     * 修改状态
     *
     * @param uid
     * @param status
     * @param ids
     * @return
     */
    @Override
    public boolean updateStatus(Long uid, int status, List<Long> ids) {

        return setmealMapper.updateStatus( uid,status,ids)>0;
    }

    /**
     * 添加套餐信息
     *
     * @param setmealDto
     * @return
     */
    @Override
    public int inset(SetmealDto setmealDto) {

        String currentId = TokenHolder.getCurrentId();
        long id = new Snowflake().nextId();
        setmealDto.setId(id);
        setmealDto.setCreateUser(Long.valueOf(currentId));
        setmealDto.setCreateTime(LocalDateTime.now());
        setmealDto.setUpdateUser(Long.valueOf(currentId));
        setmealDto.setUpdateTime(LocalDateTime.now());
        return setmealMapper.insert(setmealDto);
    }

    /**
     * getSetmealById
     *
     * @param id
     * @return
     */
    @Transactional
    @Override
    public SetmealDto getSetmealById(Long id) {

       Setmeal setmeals = setmealMapper.selectSetmealById(id);

        List<SetmealDish> setmealDishBySetmealId = setmealDishService.getSetmealDishBySetmealId(id);

        SetmealDto setmealDto = new SetmealDto();
        BeanUtil.copyProperties(setmeals,setmealDto);
        setmealDto.setSetmealDishes(setmealDishBySetmealId);

        return setmealDto;
    }

    /**
     * @param setmealDto
     * @return
     */
    @Transactional
    @Override
    public boolean updateById(SetmealDto setmealDto) {

        int i = setmealMapper.updateById(setmealDto);

        //先获取setmeal_dish删除关联
        Long setmealId = setmealDto.getId();
        boolean delete = setmealDishService.delete(setmealId);
       //再新增
        int insert = setmealDishService.insert(setmealDto);


        return i>0&&delete&&insert>0;
    }
}
