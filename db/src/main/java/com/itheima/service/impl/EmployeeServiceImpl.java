package com.itheima.service.impl;


import cn.hutool.core.lang.UUID;
import com.itheima.domain.Employee;
import com.itheima.dto.EmpAddDto;
import com.itheima.mapper.EmployeeMapper;
import com.itheima.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author CSY
 * 2022/5/7    20:55
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public Employee selectByUP(String username, String password) {

        // String s = DigestUtils.md5DigestAsHex(password.getBytes(StandardCharsets.UTF_8));
        Employee employee = employeeMapper.selectByUP(username, password);


        return employee;


    }

    /**
     * 员工分页查询
     *
     * @param page
     * @param pageSize
     * @param name     模糊查询
     * @return
     */
    @Override
    public List<Employee> selectPage(int page, int pageSize, String name) {

        page = (page - 1) * pageSize;

        List<Employee> employees = employeeMapper.selectPage(page, pageSize, name);

        return employees;

    }

    /**
     * 查询总数
     *
     * @param name
     * @return
     */
    @Override
    public int selectCount(String name) {

        name = name == null ? null : name;
        int count = employeeMapper.selectCount(name);
        return count;
    }

    /**
     * 添加的方法
     *
     * @param empAddDto
     * @return
     */
    @Override
    public boolean insert(EmpAddDto empAddDto) {

        int inset = employeeMapper.inset(empAddDto);

        return inset > 0;

    }

    /**
     * 修改状态
     *
     * @param employee
     * @return
     */
    @Override
    public boolean update(Employee employee) {

        int update = employeeMapper.update(employee);
        return update > 0;
    }

    /**
     * 根据id查询员工信息
     *
     * @param id
     * @return
     */
    @Override
    public Employee selectById(String id) {

        return employeeMapper.selectById(id);

    }
}
