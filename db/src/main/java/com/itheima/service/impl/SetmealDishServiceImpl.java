package com.itheima.service.impl;

import cn.hutool.core.lang.Snowflake;
import com.itheima.common.TokenHolder;
import com.itheima.domain.SetmealDish;
import com.itheima.dto.SetmealDto;
import com.itheima.mapper.SetmealDishMapper;
import com.itheima.service.SetmealDishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author CSY
 * 2022/5/10    21:58
 */
@Service
public class SetmealDishServiceImpl implements SetmealDishService {

    @Autowired
    private SetmealDishMapper setmealDishMapper;

    /**
     * 分页查询添加匹配外键对应name字段
     *
     * @param id
     * @return
     */
    @Override
    public String page(Long id) {

        return  null;


    }

    /**
     * 添加套餐菜品数据
     * @param setmealDto
     * @return
     */
    @Override
    public int insert(SetmealDto setmealDto) {
        Long dishId = setmealDto.getId();
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        String currentId = TokenHolder.getCurrentId();


        int insert =0;
        for (int i = 0; i < setmealDishes.size(); i++) {
            long id = new Snowflake().nextId();
            SetmealDish setmealDish = setmealDishes.get(i);
            setmealDish.setSetmealId(dishId);
            setmealDish.setId(id);
            setmealDish.setCreateUser(Long.valueOf(currentId));
            setmealDish.setCreateTime(LocalDateTime.now());
            setmealDish.setUpdateUser(Long.valueOf(currentId));
            setmealDish.setUpdateTime(LocalDateTime.now());
            insert = setmealDishMapper.insert(setmealDish);

        }
        return insert;

    }

    /**
     * getSetmealDishBySetmealId
     * @param id
     * @return
     */
    @Override
    public List<SetmealDish> getSetmealDishBySetmealId(Long id) {

        return setmealDishMapper.selectSetmealDishBySetmealId(id);
    }

    /**
     * 根据id删除套餐中的菜品
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(Long id) {

        return setmealDishMapper.deleteById(id)>0;
    }


}
