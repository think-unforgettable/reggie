package com.itheima.service.impl;

import com.itheima.domain.Category;
import com.itheima.mapper.CategoryMapper;
import com.itheima.service.CategoryService;
import com.itheima.vo.CategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    19:18
 */
@Service
public class CategoryServiceImpl implements CategoryService {

   @Autowired
   private CategoryMapper categoryMapper;
    /**
     * 分类管理分页查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public List<Category> selectPage(Integer page, Integer pageSize) {
        page = (page-1)*pageSize;
        return categoryMapper.selectpage(page,pageSize);
    }

    /**
     * 查询总数 的方法
     *
     * @return
     */
    @Override
    public int selectCount() {
       return categoryMapper.selectCount();
    }

    /**
     * 删除分类
     *
     * @param id
     * @return
     */
    @Override
    public int deleteById(Long id) {

       return categoryMapper.delete(id);

    }

    /**
     * 添加的方法
     *
     * @param category
     * @return
     */
    @Override
    public int insert(Category category) {

     return categoryMapper.insert(category);

    }

    /**
     * 根据id修改
     *
     * @param category
     * @return
     */
    @Override
    public int updateById(Category category) {

        return categoryMapper.updateById(category);
    }

    /**
     * 通过id查名称
     *
     * @param id
     * @return
     */
    @Override
    public String selectById(Long id) {

        return categoryMapper.selectById(id);
    }

    @Override
    public List<Category> selectByType(int type) {

        return categoryMapper.seleteByType(type);
    }

}
