package com.itheima.service.impl;

import com.itheima.domain.Dish;
import com.itheima.dto.DishDto;
import com.itheima.mapper.DishMapper;
import com.itheima.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    21:23
 */
@Service
public class DishServiceImpl   implements DishService {

@Autowired
private DishMapper dishMapper;
    /**
     * 分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public List<Dish> page(int page, int pageSize, String name) {
        page = (page-1)*pageSize;

        return  dishMapper.page(page, pageSize, name);

    }

    /**
     * 查总数
     *
     * @return
     */
    @Override
    public int count(String name) {
        return dishMapper.selectCount(name);
    }

    /**
     * 修改状态
     *
     * @param uid
     * @param status
     * @param ids
     * @return
     */
    @Override
    public int updateStatus(Long uid, int status, List<Long> ids) {

        return dishMapper.updateStatus(uid,status,ids);

    }

    /**
     * 删除菜品
     *
     * @param ids
     * @return
     */
    @Override
    public int deleteByIds(List<Long> ids) {

        return dishMapper.deleteByIds(ids);


    }

    /**
     * 添加方法
     *
     * @param dish
     * @return
     */
    @Override
    public int insert(Dish dish) {

        return  dishMapper.insert(dish);

    }

    /**
     * 查单个
     *
     * @param id
     * @return
     */
    @Override
    public Dish byId(Long id) {

        return   dishMapper.byId(id);

    }

    /**
     * 修改的方法
     *
     * @param dish
     * @return
     */
    @Override
    public int update(Dish dish) {

        return  dishMapper.update(dish);

    }

    /**
     * selectDishcategoryId
     *
     * @param id
     * @return
     */
    @Override
    public List<Dish> selectDishcategoryId(Long id) {

        List<Dish> dishes = dishMapper.selectDishcategoryId(id);

        return dishes;
    }
}
