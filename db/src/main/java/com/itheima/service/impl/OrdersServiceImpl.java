package com.itheima.service.impl;

import cn.hutool.core.date.DateTime;
import com.itheima.domain.Orders;
import com.itheima.mapper.OrdersMapper;
import com.itheima.service.OrdersService;
import com.itheima.vo.OrdersVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CSY
 * 2022/5/11    15:39
 */
@Service
public class OrdersServiceImpl implements OrdersService {

   @Autowired
   private OrdersMapper ordersMapper;

    /**
     * 分页查询
     *
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @Override
    public OrdersVo page(int page, int pageSize, String number, String beginTime, String endTime) {

        page=(page-1)*pageSize;
        List<Orders> pageInfo = ordersMapper.page(page, pageSize, number, beginTime, endTime);

        OrdersVo ordersVo = new OrdersVo();
        ordersVo.setRecords(pageInfo);
        ordersVo.setSize(pageSize);
        ordersVo.setCurrent(page);
        ordersVo.setTotal(count(number));

        return ordersVo;
    }

    /**
     * 查总数
     *
     * @param number
     * @return
     */
    @Override
    public int count(String number) {
        return ordersMapper.count(number);
    }

    /**
     * 修改订单的状态
     *
     * @param orders
     * @return
     */
    @Override
    public boolean updateStatus(Orders orders) {

        int i = ordersMapper.updateStatus(orders);


        return i>0;
    }
}
