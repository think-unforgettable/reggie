package com.itheima.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.itheima.domain.Dish;
import com.itheima.domain.DishFlavor;
import com.itheima.dto.DishDto;
import com.itheima.dto.UserDishDto;
import com.itheima.mapper.UserDishMapper;
import com.itheima.service.DishFlavorService;
import com.itheima.service.DishService;
import com.itheima.service.UserDishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author CSY
 * 2022/5/12    16:13
 */
@Service
public class UserDishServiceImpl implements UserDishService {

  @Autowired
  private UserDishMapper userDishMapper;
  @Autowired
  private DishFlavorService dishFlavorService;

    /**
     * 前台分类中菜品展示
     *
     * @param categoryId
     * @param status
     * @return
     */
    @Override
    public List<DishDto> selectAll(Long categoryId, int status) {

      List<Dish> dishes = userDishMapper.listDish(categoryId, status);

      List<DishDto> dishDtos=dishes.stream().filter(Objects::nonNull).map((res)->{
        DishDto dishDto = new DishDto();
        BeanUtil.copyProperties(res,dishDto);
        Long dishId = res.getId();
        List<DishFlavor> dishFlavors = dishFlavorService.byId(dishId);
        dishDto.setFlavors(dishFlavors);

        return dishDto;

      }).collect(Collectors.toList());




      return dishDtos;
    }
}
