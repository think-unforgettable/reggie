package com.itheima.service.impl;

import cn.hutool.core.lang.Snowflake;
import com.itheima.dto.UserDto;
import com.itheima.mapper.UserMapper;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author CSY
 * 2022/5/12    11:46
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    /**
     * 查询用户存在否？不存在则创建
     *
     * @param phone
     * @return
     */
    @Override
    public UserDto selectByPhone(String phone) {



        return userMapper.selectByPhone(phone);
    }

    /**
     * 创建新用户
     *
     * @param userDto
     * @return
     */
    @Override
    public boolean insert(UserDto userDto) {

        long id = new Snowflake().nextId();
        userDto.setId(id);
        userDto.setStatus(1);

        return userMapper.insertUser(userDto)>0;
    }
}
