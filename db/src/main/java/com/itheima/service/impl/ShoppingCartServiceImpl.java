package com.itheima.service.impl;

import cn.hutool.core.lang.Snowflake;
import com.itheima.common.R;
import com.itheima.common.TokenHolder;
import com.itheima.domain.ShoppingCart;
import com.itheima.mapper.ShoppingCartMapper;
import com.itheima.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author CSY
 * 2022/5/12    15:48
 */
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

   @Autowired
   private ShoppingCartMapper shoppingCartMapper;
    /**
     * 购物车查询
     *
     * @return
     */
    @Override
    public List<ShoppingCart> shoppingCarts() {

        String currentId = TokenHolder.getCurrentId();
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.shoppingCarts(Long.valueOf(currentId));


        return shoppingCarts;
    }

    /**
     * 添加到购物车
     *
     * @param shoppingCart
     * @return
     */
    @Override
    public boolean shoppingCartAdd(ShoppingCart shoppingCart) {

        long id = new Snowflake().nextId();
        shoppingCart.setId(id);

        shoppingCart.setCreateTime(LocalDateTime.now());
        int i = shoppingCartMapper.shoppingCartAdd(shoppingCart);

        return i>0;
    }


    /**
     * 查询是否存在
     *
     * @param shoppingCart
     * @return
     */
    @Override
    public boolean getdishByUidDishid01(ShoppingCart shoppingCart) {

        int i = shoppingCartMapper.getdishByUidDishid(shoppingCart);


        return i>0;
    }

    /**
     * 修改在购物车中的份数
     *
     * @param shoppingCart
     * @return
     */

    @Override
    public boolean updateNumber(ShoppingCart shoppingCart) {
        //TODO number的动态获取  再次查询

        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectNumber(shoppingCart);
        int i =0;
        for (ShoppingCart cart : shoppingCarts) {
            Integer number = cart.getNumber();
            shoppingCart.setNumber(number+1);
            i = shoppingCartMapper.updateNumber(shoppingCart);
        }



        return i>0;
    }

    /**
     * selectShoppCartByUserId
     *
     *
     * @return
     */
    @Override
    public List<ShoppingCart> selectShoppCartByUserId() {

        String currentId = TokenHolder.getCurrentId();
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectShoppCartByUserId(Long.valueOf(currentId));


        return shoppingCarts;
    }

    /**
     * 清空购物车
     *
     * @return
     */
    @Transactional
    @Override
    public boolean delete() {
        String currentId = TokenHolder.getCurrentId();
        int count = shoppingCartMapper.count(Long.valueOf(currentId));
        int delect = shoppingCartMapper.delect(Long.valueOf(currentId));

        return delect==count;
    }

    /**
     * @param shoppingCart
     * @return
     */
    @Override
    public boolean sub(ShoppingCart shoppingCart) {
        String currentId = TokenHolder.getCurrentId();
        shoppingCart.setUserId(Long.valueOf(currentId));

        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectNumber(shoppingCart);
        int sub =0;
        int i=0;
        for (ShoppingCart cart : shoppingCarts) {
            Integer number = cart.getNumber();
            if (number==1){
                sub = shoppingCartMapper.sub(shoppingCart);
            }
            shoppingCart.setNumber(number-1);
             i = shoppingCartMapper.subOne(shoppingCart);
        }





        return sub>0||i>0;
    }



}
