package com.itheima.service.impl;

import com.itheima.domain.DishFlavor;
import com.itheima.mapper.DishFlavorMapper;
import com.itheima.service.DishFlavorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CSY
 * 2022/5/10    13:26
 */
@Service
public class DishFlavorServiceImpl implements DishFlavorService {

   @Autowired
   private DishFlavorMapper dishFlavorMapper;
    /**
     * 添加
     *
     * @param dishFlavor
     * @return
     */
    @Override
    public int insert(DishFlavor dishFlavor) {
        return dishFlavorMapper.insert(dishFlavor);
    }

    /**
     * 查单个
     *
     * @param id
     * @return
     */
    @Override
    public List<DishFlavor> byId(Long id) {

        return dishFlavorMapper.byDishId(id);

    }

    /**
     * 修改
     *
     * @param dishFlavor
     * @return
     */
    @Override
    public int update(DishFlavor dishFlavor) {
        return dishFlavorMapper.update(dishFlavor);

    }

}
