package com.itheima.service.impl;

import cn.hutool.core.lang.Snowflake;
import com.itheima.common.R;
import com.itheima.common.TokenHolder;
import com.itheima.domain.AddressBook;
import com.itheima.mapper.UserAddressBookMapper;
import com.itheima.service.UserAddressBookService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author CSY
 * 2022/5/13    20:50
 */
@Service
public class UserAddressBookServiceImpl implements UserAddressBookService {

    @Autowired
    private UserAddressBookMapper uabm;

    /**
     * 添加地址
     *
     * @param addressBook
     * @return
     */
    @Override
    public AddressBook addAddress(AddressBook addressBook) {

        long id = new Snowflake().nextId();
        String currentId = TokenHolder.getCurrentId();
        addressBook.setId(id);
        addressBook.setUserId(Long.valueOf(currentId));
        addressBook.setCreateUser(Long.valueOf(currentId));
        addressBook.setCreateTime(LocalDateTime.now());
        addressBook.setUpdateUser(Long.valueOf(currentId));
        addressBook.setUpdateTime(LocalDateTime.now());
        addressBook.setIsDefault(0);

        int i = uabm.insertAddress(addressBook);

        return i>0? addressBook:null;
    }

    /**
     * 收货地址列表
     *
     * @return
     */
    @Override
    public List<AddressBook> getAllAddress() {

        String currentId = TokenHolder.getCurrentId();

        return  uabm.selectAddress(Long.valueOf(currentId));

    }

    /**
     * 设置收货地址
     *
     * @return
     */
    @Transactional
    @Override
    public AddressBook setdefault(Long id) {
        String currentId = TokenHolder.getCurrentId();

        int i = uabm.updateDefaults(Long.valueOf(currentId));
        int i1 = uabm.updateDefault(id, Long.valueOf(currentId));
        AddressBook addressBook = uabm.selectAddressDefault(Long.valueOf(currentId));

        return addressBook;
    }

    /**
     * getAddress
     *
     * @return
     */
    @Override
    public AddressBook getAddress(Long id) {

        String currentId = TokenHolder.getCurrentId();

        return uabm.updatelist(id, Long.valueOf(currentId));
    }

    /**
     * 修改地址
     *
     * @param addressBook
     * @return
     */
    @Override
    public boolean updateAddress(AddressBook addressBook) {

        int i = uabm.updateAddress(addressBook);

        return i>0;
    }

    /**
     * 删除的方法
     *
     * @param ids
     * @return
     */
    @Override
    public boolean deleteAddress(Long ids) {
        String currentId = TokenHolder.getCurrentId();

        int i = uabm.deleteAddress(Long.valueOf(currentId), ids);

        return i>0;
    }

    /**
     * 获取默认地址
     *
     * @return
     */
    @Override
    public AddressBook getDefaultAddress() {

        String currentId = TokenHolder.getCurrentId();
        AddressBook addressBook = uabm.selectAddressDefault(Long.valueOf(currentId));
        return addressBook!=null?addressBook:null;
    }


}
