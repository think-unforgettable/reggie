package com.itheima.service.impl;

import com.itheima.domain.Setmeal;
import com.itheima.domain.SetmealDish;
import com.itheima.mapper.UserSetmealMapper;
import com.itheima.service.UserSetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    18:04
 */
@Service
public class UserSetmealServiceImpl implements UserSetmealService {

    @Autowired
    private UserSetmealMapper userSetmealMapper;

    /**
     * 前台获取套餐分类中的所有套餐
     *
     * @param categoryId
     * @param status
     * @return
     */
    @Override
    public List<Setmeal> getSetmealAll(Long categoryId, int status) {

        List<Setmeal> setmeals = userSetmealMapper.selectSetmeal(categoryId, status);

        return setmeals;
    }

    /**
     * 返回套餐菜品信息
     *
     * @param setmealId
     * @return
     */
    @Override
    public List<SetmealDish> getSetmealDish(Long setmealId) {

        List<SetmealDish> setmealDishs = userSetmealMapper.getSetmealDish(setmealId);
        return setmealDishs;
    }
}
