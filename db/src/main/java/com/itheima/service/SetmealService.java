package com.itheima.service;

import com.itheima.domain.Setmeal;
import com.itheima.dto.SetmealDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/10    21:18
 */

public interface SetmealService {
    /**
     * 分页查询
     * @param name
     * @param page
     * @param pageSize
     * @return
     */
    List<Setmeal>  page(String name ,int page , int pageSize);

    /**
     * 查总数
     * @param name
     * @return
     */
    int selectAll(String name);

    /**
     * 按ids删除
      * @param ids
     * @return
     */
    boolean deleteByIds(List<Long> ids);

    /**
     * 修改状态
     * @param uid
     * @param status
     * @param ids
     * @return
     */
    boolean updateStatus(Long uid, int status, List<Long> ids);

    /**
     * 添加套餐信息
     * @param setmealDto
     * @return
     */
    int inset(SetmealDto setmealDto);

    /**
     * getSetmealById
     * @param id
     * @return
     */
    SetmealDto getSetmealById(Long id);

    /**
     *
     * @return
     */
    boolean updateById(SetmealDto setmealDto);



}
