package com.itheima.service;

import com.itheima.domain.Setmeal;
import com.itheima.domain.SetmealDish;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    18:02
 */
public interface UserSetmealService {
    /**
     * 前台获取套餐分类中的所有套餐
     * @param categoryId
     * @param status
     * @return
     */
    List<Setmeal> getSetmealAll(Long categoryId,int status);

    /**
     * 返回套餐菜品信息
     * @return
     */
    List<SetmealDish> getSetmealDish(Long setmealId);
}
