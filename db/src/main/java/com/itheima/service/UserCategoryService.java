package com.itheima.service;

import com.itheima.domain.Category;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    15:12
 */
public interface UserCategoryService {
    /**
     * 查前台所有
     * @return
     */
    List<Category> getAllCategory();

}
