package com.itheima.service;

import com.itheima.domain.AddressBook;

import java.util.List;

/**
 * @author CSY
 * 2022/5/13    20:49
 */

public interface UserAddressBookService {

    /**
     * 添加地址
     * @param addressBook
     * @return
     */
    AddressBook addAddress(AddressBook addressBook);

    /**
     * 收货地址列表
     * @return
     */
    List<AddressBook> getAllAddress();


    /**
     * 设置收货地址
     * @return
     */
    AddressBook setdefault(Long id);

    /**
     * getAddress
     * @return
     */
    AddressBook getAddress(Long id);

    /**
     * 修改地址
     * @param addressBook
     * @return
     */
    boolean updateAddress(AddressBook addressBook);

    /**
     * 删除的方法
     * @param ids
     * @return
     */
    boolean deleteAddress(Long ids);

    /**
     * 获取默认地址
     * @return
     */
    AddressBook getDefaultAddress();
}
