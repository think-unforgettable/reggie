package com.itheima.service;

import com.itheima.domain.ShoppingCart;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    15:46
 */

public interface ShoppingCartService {

    /**
     * 购物车查询
     * @return
     */
    List<ShoppingCart> shoppingCarts();

    /**
     * 添加到购物车
     * @param shoppingCart
     * @return
     */
    boolean shoppingCartAdd(ShoppingCart shoppingCart);

    /**
     * 查询是否存在
     * @param shoppingCart
     * @return
     */
    boolean getdishByUidDishid01(ShoppingCart shoppingCart);

    /**
     * 修改在购物车中的份数
     * @param shoppingCart
     * @return
     */
    boolean updateNumber(ShoppingCart shoppingCart);






    /**
     * selectShoppCartByUserId
     *
     * @return
     */
    List<ShoppingCart> selectShoppCartByUserId();

    /**
     * 清空购物车
     * @return
     */
    boolean delete();

    /**
     *
     * @param shoppingCart
     * @return
     */
    boolean sub(ShoppingCart shoppingCart);


}
