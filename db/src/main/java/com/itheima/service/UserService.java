package com.itheima.service;

import com.itheima.dto.UserDto;

/**
 * @author CSY
 * 2022/5/12    11:46
 */
public interface UserService {
    /**
     * 查询用户存在否？不存在则创建
     * @param phone
     * @return
     */
    UserDto selectByPhone(String phone);

    /**
     * 创建新用户
     * @param userDto
     * @return
     */
    boolean insert (UserDto userDto);

}
