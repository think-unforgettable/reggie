package com.itheima.service;

import com.itheima.dto.DishDto;
import com.itheima.dto.UserDishDto;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    16:11
 */

public interface UserDishService {

    /**
     * 前台分类中菜品展示
     * @param categoryId
     * @param status
     * @return
     */
    List<DishDto> selectAll(Long categoryId, int status);
}
