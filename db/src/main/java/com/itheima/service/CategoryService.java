package com.itheima.service;

import com.itheima.domain.Category;
import com.itheima.vo.CategoryVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    19:16
 */

public interface CategoryService {
    /**
     * 分类管理分页查询
     * @param page
     * @param pageSize
     * @return
     */
    List<Category> selectPage(Integer page, Integer pageSize);

    /**
     * 查询总数 的方法
     * @return
     */
    int selectCount();

    /**
     * 删除分类
     * @param id
     * @return
     */
    int deleteById(Long id);

    /**
     * 添加的方法
     * @param category
     * @return
     */
    int insert(Category category);

    /**
     * 根据id修改
     * @param category
     * @return
     */
    int updateById(Category category);

    /**
     * 通过id查名称
     * @param id
     * @return
     */
    String selectById(Long id);

    /**
     * 下拉列表
     * @param type
     * @return
     */
    List<Category> selectByType(int type);
}
