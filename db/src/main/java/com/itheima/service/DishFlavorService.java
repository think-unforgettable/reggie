package com.itheima.service;

import com.itheima.domain.DishFlavor;

import java.util.List;

/**
 * @author CSY
 * 2022/5/10    13:25
 */

public interface DishFlavorService {

    /**
     * 添加
     * @param dishFlavor
     * @return
     */
    int insert(DishFlavor dishFlavor);

    /**
     * 查单个
     * @param id
     * @return
     */
    List<DishFlavor> byId(Long id);

    /**
     * 修改
     * @param dishFlavor
     * @return
     */
    int update(DishFlavor dishFlavor);
}
