package com.itheima.service;

import com.itheima.domain.Dish;
import com.itheima.dto.DishDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    21:20
 */

public interface DishService {
    /**
     * 分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    List<Dish> page(int page , int pageSize , String name);

    /**
     * 查总数
     * @return
     */
    int count(String name );

    /**
     * 修改状态
     * @param uid
     * @param status
     * @param ids
     * @return
     */
    int updateStatus(Long uid,int status,List<Long> ids);

    /**
     * 删除菜品
     * @param ids
     * @return
     */
    int deleteByIds(List<Long> ids);

    /**
     * 添加方法
     * @param dish
     * @return
     */
    int insert(Dish dish);

    /**
     * 查单个
     * @param id
     * @return
     */
    Dish byId(Long id);

    /**
     * 修改的方法
     * @param dish
     * @return
     */
    int update(Dish dish);

    /**
     * selectDishcategoryId
     * @param id
     * @return
     */
    List<Dish> selectDishcategoryId(Long id);
}
