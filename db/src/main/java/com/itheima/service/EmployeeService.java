package com.itheima.service;

import com.itheima.domain.Employee;
import com.itheima.dto.EmpAddDto;

import java.util.List;

/**
 * @author CSY
 * 2022/5/7    20:55
 */

public interface EmployeeService {

    /**
     *          登录
     * @param username
     * @param password
     * @return
     */
    Employee selectByUP(String username,String password);

    /**
     *          员工分页查询
     * @param page
     * @param pageSize
     * @param name 模糊查询
     * @return
     */
    List<Employee> selectPage(int page,int pageSize,String name);

    /**
     *  查询总数
     * @param name
     * @return
     */
    int selectCount(String name);

    /**
     * 添加的方法
     * @param empAddDto
     * @return
     */
    boolean insert(EmpAddDto empAddDto);

    /**
     *      修改状态
     * @param employee
     * @return
     */
    boolean update(Employee employee);

    /**
     * 根据id查询员工信息
     * @param id
     * @return
     */
    Employee selectById(String id);
}
