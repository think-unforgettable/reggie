package com.itheima.service;

import cn.hutool.core.date.DateTime;
import com.itheima.domain.Orders;
import com.itheima.vo.OrdersVo;

import java.util.List;

/**
 * @author CSY
 * 2022/5/11    15:38
 */

public interface OrdersService {
    /**
     * 分页查询
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    OrdersVo page(int page, int pageSize, String number, String beginTime , String endTime);

    /**
     * 查总数
     * @param number
     * @return
     */
    int count(String number);


    /**
     * 修改订单的状态
     * @param orders
     * @return
     */
    boolean updateStatus(Orders orders);
}
