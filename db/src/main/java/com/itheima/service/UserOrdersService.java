package com.itheima.service;

import com.itheima.domain.Orders;
import com.itheima.vo.UserOrdersVo;

/**
 * @author CSY
 * 2022/5/14    14:51
 */
public interface UserOrdersService {

    /**
     * 添加订单信息
     * @param orders
     * @return
     */
    boolean addOrders(Orders orders);

    /**
     * 订单表分页查询
     * @return
     */
    UserOrdersVo getOrders(int page ,int pageSize);
}
