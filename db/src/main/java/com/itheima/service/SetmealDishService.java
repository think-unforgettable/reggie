package com.itheima.service;

import com.itheima.domain.SetmealDish;
import com.itheima.dto.SetmealDto;

import java.util.List;

/**
 * @author CSY
 * 2022/5/10    21:56
 */

public interface SetmealDishService {
    /**
     * 分页查询添加匹配外键对应name字段
     * @param id
     * @return
     */
    String page(Long id);

    /**
     * 添加套餐菜品数据
     * @param setmealDto
     * @return
     */
    int insert(SetmealDto setmealDto);

    /**
     * 根据id查单个
     * @param id
     * @return
     */
    List<SetmealDish> getSetmealDishBySetmealId(Long id);

    /**
     * 根据id删除套餐中的菜品
     * @param id
     * @return
     */
    boolean delete(Long id);

}
