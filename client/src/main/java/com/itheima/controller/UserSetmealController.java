package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.domain.Setmeal;
import com.itheima.domain.SetmealDish;
import com.itheima.service.UserSetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    17:27
 */
@RestController
@Slf4j
@RequestMapping("/setmeal")
public class UserSetmealController {

    @Autowired
    private UserSetmealService userSetmealService;

    @GetMapping("/list")
    public R<List<Setmeal>> setmeals(Long categoryId ,int status ){

        List<Setmeal> setmealAll = userSetmealService.getSetmealAll(categoryId, status);

        return R.success(setmealAll);
    }

    @GetMapping("/dish/{setmealId}")
    public R<List<SetmealDish>> getSetmealDish(@PathVariable Long setmealId){

        List<SetmealDish> setmealDishs = userSetmealService.getSetmealDish(setmealId);

        return setmealDishs!=null?R.success(setmealDishs):R.error("???");
    }
}
