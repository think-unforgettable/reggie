package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.domain.Category;
import com.itheima.service.UserCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    15:02
 */
@RestController
@Slf4j
@RequestMapping("/category")
public class UserCategoryController {

    @Autowired
    private UserCategoryService userCategoryService;

    @GetMapping("/list")
    public R<List<Category>> list(){

        List<Category> allCategory = userCategoryService.getAllCategory();

        return R.success(allCategory);
    }

}
