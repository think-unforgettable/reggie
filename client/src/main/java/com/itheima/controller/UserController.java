package com.itheima.controller;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.itheima.common.R;
import com.itheima.constant.TokenConstant;
import com.itheima.dto.UserDto;
import com.itheima.service.UserService;
import com.itheima.vo.UserTokenVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * @author CSY
 * 2022/5/12    10:32
 */
@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {

    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;

    @Autowired
    private UserService userService;


    /**
     * 登录
     * @param userDto
     * @return
     */

    @PostMapping("/login")
    public R<UserTokenVo> login(@RequestBody UserDto userDto){

        assert userDto!=null;
        String userPhone = userDto.getPhone();
        //获取两个code
        String code = userDto.getCode();
        String codeNumber = (String) redisTemplate.opsForValue().get(userPhone);


        //做对比
        if (code.equals(codeNumber)){

            UserDto userDto1 = userService.selectByPhone(userPhone);
            if (userDto1 == null){
                long id = new Snowflake().nextId();
                userDto.setId(id);
                userService.insert(userDto);
            }


                String tokenString = System.currentTimeMillis() + userPhone;
                String token = DigestUtils.md5DigestAsHex(tokenString.getBytes());
                redisTemplate.opsForValue().set(TokenConstant.USER_TOKEN_PREFIX+token, JSON.toJSONString(userDto1),48L, TimeUnit.HOURS);

                UserTokenVo userTokenVo = new UserTokenVo();
                BeanUtils.copyProperties(userDto,userTokenVo);
                userTokenVo.setToken(token);

                return R.success(userTokenVo);


        }




        return R.error("验证码错误");
    }

    /**
     * 消息验证
     * @param userDto
     */
    @PostMapping("/sendMsg")
    public void sendMsg(@RequestBody UserDto userDto){
        //获取需要验证码的phone
        String userPhone = userDto.getPhone();

        int codeNumber = RandomUtil.randomInt(1000, 9999);

        String code = String.valueOf(codeNumber);
        log.info("==================================>"+code);
        redisTemplate.opsForValue().set(userPhone,code);


    }

    @PostMapping("/loginout")
    public R<String> logout(HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("AuthorizationUser");
        redisTemplate.delete(TokenConstant.USER_TOKEN_PREFIX+token);
        return R.success("退出成功");
    }

}
