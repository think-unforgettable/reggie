package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.domain.Orders;
import com.itheima.service.UserOrdersService;
import com.itheima.vo.UserOrdersVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CSY
 * 2022/5/14    11:40
 */
@RestController
@RequestMapping("/order")
@Slf4j
public class UserOrderController {

    @Autowired
    private UserOrdersService userOrdersService;

    @GetMapping("/userPage")
    public R<UserOrdersVo> page(int page , int pageSize ){

        UserOrdersVo orders = userOrdersService.getOrders(page, pageSize);

        return orders!=null?R.success(orders):R.error("失败");
    }

    /**
     * 提交订单
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){

        //TODO 提交购物车的同时，取出购物车讲购物车的数据填入订单明细表。

        boolean b = userOrdersService.addOrders(orders);


        return b?R.success("添加成功"):R.error("添加失败");
    }


}
