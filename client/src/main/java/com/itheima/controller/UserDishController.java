package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.dto.DishDto;
import com.itheima.dto.UserDishDto;
import com.itheima.service.UserDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author CSY
 * 2022/5/12    15:59
 */
@RequestMapping("/dish")
@Slf4j
@RestController
public class UserDishController {

    @Autowired
    private  UserDishService userDishService;

    @GetMapping("/list")
    public   R<List<DishDto>> dishs(Long categoryId, int status){

        List<DishDto> dishDtos = userDishService.selectAll(categoryId, status);

        if (dishDtos!=null){
            return R.success(dishDtos);
        }
        return R.error("数据出错了");
    }


}
