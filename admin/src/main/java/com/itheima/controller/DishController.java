package com.itheima.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import com.itheima.common.R;
import com.itheima.common.TokenHolder;
import com.itheima.domain.Dish;
import com.itheima.domain.DishFlavor;
import com.itheima.dto.DishDto;
import com.itheima.service.CategoryService;
import com.itheima.service.DishFlavorService;
import com.itheima.service.DishService;
import com.itheima.vo.DishVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author CSY
 * 2022/5/9    21:03
 */
@RestController
@Slf4j
@RequestMapping("/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DishFlavorService dishFlavorService;

    /**
     * 菜品分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<DishVo> page(int page,int pageSize,String name){
        //菜品信息
        List<Dish> dishes = dishService.page(page, pageSize, name);

        List<DishDto> dishDtos=dishes.stream().map((res)->{
            DishDto dishDto = new DishDto();
            BeanUtil.copyProperties(res,dishDto);
            //获取dish的CategoryId
            Long categoryId = res.getCategoryId();
            //通过id得到名称
            String s = categoryService.selectById(categoryId);

            dishDto.setCategoryName(s);

            // dishFlavor.set
            return dishDto;
        }).collect(Collectors.toList());

        //菜品总数
        int dishCount = dishService.count(name);

        DishVo dishVo = new DishVo();
        dishVo.setRecords(dishDtos);
        dishVo.setCurrent(page);
        dishVo.setTotal(dishCount);
        dishVo.setSize(pageSize);



        return R.success(dishVo);
    }

    @PostMapping("/status/{status}")
    public R<String> updateStatus(@PathVariable int status,@RequestParam List<Long> ids){

        //修改人
        String currentId = TokenHolder.getCurrentId();

        int i = dishService.updateStatus(Long.valueOf(currentId), status, ids);

        return i>0?R.success("修改成功"):R.error("修改失败");
    }

    @DeleteMapping
    public R<String> deleteByIds(@Param("ids") List<Long> ids){

        int i = dishService.deleteByIds(ids);
        return i>0?R.success("删除成功") : R.error("删除失败");
    }


    @PostMapping
    public R<String> addDish(@RequestBody DishDto dishDto){

        Dish dish = new Dish();
        String dishId = new Snowflake().nextIdStr();
        String currentId = TokenHolder.getCurrentId();

        dish.setCode("10001");
        dish.setStatus(1);
        dish.setPrice(dishDto.getPrice());
        dish.setCategoryId(dishDto.getCategoryId());
        dish.setDescription(dishDto.getDescription());
        dish.setImage(dishDto.getImage());
        dish.setName(dishDto.getName());
        dish.setId(Long.valueOf(dishId));
        dish.setCreateUser(Long.valueOf(currentId));
        dish.setUpdateUser(Long.valueOf(currentId));
        dish.setCreateTime(LocalDateTime.now());
        dish.setUpdateTime(LocalDateTime.now());
        dishService.insert(dish);


        List<DishFlavor> flavors = dishDto.getFlavors();

        for (int i = 0; i < flavors.size(); i++) {
            DishFlavor dishFlavor=  flavors.get(i);
            dishFlavor.setId(Long.parseLong(currentId)+1);
            dishFlavor.setDishId(Long.valueOf(currentId));
            dishFlavor.setCreateUser(Long.valueOf(currentId));
            dishFlavor.setUpdateUser(Long.valueOf(currentId));
            dishFlavor.setCreateTime(LocalDateTime.now());
            dishFlavor.setUpdateTime(LocalDateTime.now());
            dishFlavorService.insert(dishFlavor);
        }

        return R.success("添加成功");
    }

    /**
     * 查单个
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R<DishDto> byId(@PathVariable Long id){

        DishDto dishDto = new DishDto();
        Dish dish = dishService.byId(id);
        List<DishFlavor> dishFlavors = dishFlavorService.byId(id);
        dishDto.setFlavors(dishFlavors);
        BeanUtil.copyProperties(dish,dishDto);


        return R.success(dishDto);
    }

    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto){

        Dish dish = new Dish();
        List<DishFlavor> flavors = dishDto.getFlavors();
        for (int i = 0; i < flavors.size(); i++) {
            DishFlavor dishFlavor = flavors.get(i);
            dishFlavorService.update(dishFlavor);
        }

        BeanUtil.copyProperties(dishDto,dish);
        dishService.update(dish);

        return R.success("成功");

    }


    @GetMapping("/list")
    public R<List<Dish>> listByCategoryId(Long categoryId){

        List<Dish> dishes = dishService.selectDishcategoryId(categoryId);

        return R.success(dishes);
    }
}
