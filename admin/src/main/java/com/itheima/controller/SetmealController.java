package com.itheima.controller;

import cn.hutool.core.bean.BeanUtil;
import com.itheima.common.R;
import com.itheima.common.TokenHolder;
import com.itheima.domain.Setmeal;
import com.itheima.domain.SetmealDish;
import com.itheima.dto.SetmealDto;
import com.itheima.service.CategoryService;
import com.itheima.service.SetmealDishService;
import com.itheima.service.SetmealService;
import com.itheima.vo.SetmealVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author CSY
 * 2022/5/10    21:04
 */
@RestController
@Slf4j
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/page")
    public R<SetmealVo> page(String name, int page, int pageSize) {


        List<Setmeal> setmeals = setmealService.page(name, page, pageSize);
       /* List<SetmealDto> setmealDtos = new ArrayList<>();
        for (int i = 0; i < setmeals.size(); i++) {

            SetmealDto setmealDto = new SetmealDto();
            BeanUtil.copyProperties(setmeals.get(i),setmealDto);
            Long id = setmeals.get(i).getCategoryId();
            String s = categoryService.selectById(id);
            setmealDto.setCategoryName(s);
            setmealDtos.add(setmealDto);
        }*/


        List<SetmealDto> setmealDtos = setmeals.stream().map((res) -> {
            SetmealDto setmealDto = new SetmealDto();

            BeanUtil.copyProperties(res, setmealDto);
            Long id = res.getCategoryId();

            String s = categoryService.selectById(id);
            setmealDto.setCategoryName(s);

            return setmealDto;
        }).collect(Collectors.toList());

        SetmealVo setmealVo = new SetmealVo();
        setmealVo.setRecords(setmealDtos);
        setmealVo.setCurrent(page);
        setmealVo.setSize(pageSize);
        setmealVo.setTotal(setmealService.selectAll(name));


        return R.success(setmealVo);
    }


    @DeleteMapping
    public R<String> delete(@RequestParam List<Long> ids) {

        return setmealService.deleteByIds(ids) ? R.success("成功删除") : R.error("删除失败");
    }

    @PostMapping("/status/{status}")
    public R<String> updateStatus(@PathVariable int status, @RequestParam List<Long> ids) {

        String currentId = TokenHolder.getCurrentId();

        return setmealService.updateStatus(Long.valueOf(currentId), status, ids) ? R.success("状态修改成功") : R.error("失败");
    }

    @PostMapping
    public R<String> add(@RequestBody SetmealDto setmealDto) {

        int setmealCount = setmealService.inset(setmealDto);
        int setmealDishCount = setmealDishService.insert(setmealDto);

        return setmealCount > 0 && setmealDishCount > 0 ? R.success("添加成功") : R.error("添加失败");
    }


    @GetMapping("/{id}")
    public R<SetmealDto> getSetmealById(@PathVariable Long id) {

        SetmealDto setmealById = setmealService.getSetmealById(id);


        return setmealById != null ? R.success(setmealById) : R.error("数据有误");
    }

    @PutMapping()
    public R<String> update(@RequestBody SetmealDto setmealDto) {

        boolean b = setmealService.updateById(setmealDto);

        return b?R.success("修改成功"):R.error("数据出现错误");
    }


}
