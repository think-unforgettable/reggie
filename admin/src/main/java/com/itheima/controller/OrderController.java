package com.itheima.controller;

import cn.hutool.core.date.DateTime;
import com.itheima.common.R;
import com.itheima.domain.Orders;
import com.itheima.dto.OrdersDto;
import com.itheima.service.OrdersService;
import com.itheima.vo.OrdersVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author CSY
 * 2022/5/11    14:37
 */
@RestController
@Slf4j
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrdersService ordersService;


    @GetMapping("/page")
    public R<OrdersVo> page(int page, int pageSize, String number, String beginTime , String endTime ){

        OrdersVo pageInfo = ordersService.page(page, pageSize, number, beginTime, endTime);


        return pageInfo!=null? R.success(pageInfo):R.error("查询失败");
    }

    @PutMapping
    public R<String> updateStatus(@RequestBody Orders orders){

        boolean b = ordersService.updateStatus(orders);

        return b? R.success(null):R.error(null);
    }

}
