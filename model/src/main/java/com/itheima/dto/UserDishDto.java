package com.itheima.dto;


import com.itheima.domain.Dish;
import com.itheima.domain.DishFlavor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;


@Data
public class UserDishDto {

    private List<DishDto> records;

    //private String categoryName;

    private Integer copies;
}
