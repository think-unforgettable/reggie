package com.itheima.dto;

import com.itheima.domain.User;
import lombok.Data;

/**
 * @author CSY
 * 2022/5/12    10:39
 */

@Data
public class UserDto extends User {

    private String code;

}
