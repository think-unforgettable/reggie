package com.itheima.dto;

import com.itheima.domain.Employee;
import lombok.Data;

/**
 * @author CSY
 * 2022/5/9    16:28
 */
@Data
public class EmpAddDto extends Employee {
}
