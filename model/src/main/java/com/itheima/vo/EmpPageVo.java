package com.itheima.vo;

import com.itheima.domain.Employee;
import lombok.Data;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    11:14
 */
@Data
public class EmpPageVo {

    private List<Employee> records;

    private Integer total;
    private Integer size;
    private Integer current;

}
