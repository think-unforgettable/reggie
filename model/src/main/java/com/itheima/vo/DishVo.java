package com.itheima.vo;

import com.itheima.domain.Dish;
import com.itheima.dto.DishDto;
import lombok.Data;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    21:08
 */
@Data
public class DishVo {
    private List<DishDto> records;
    private Integer total;
    private Integer current;
    private Integer size;

}
