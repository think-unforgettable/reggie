package com.itheima.vo;

import com.itheima.domain.Category;
import lombok.Data;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    19:05
 */
@Data
public class CategoryVo {



    private List<Category> records;





    private Integer total;
    private Integer current;
    private Integer size;
}
