package com.itheima.vo;

import com.itheima.domain.Orders;
import com.itheima.dto.DishDto;
import lombok.Data;

import java.util.List;

/**
 * @author CSY
 * 2022/5/11    15:11
 */
@Data
public class OrdersVo {

    private List<Orders> records;
    private Integer total;
    private Integer current;
    private Integer size;

}
