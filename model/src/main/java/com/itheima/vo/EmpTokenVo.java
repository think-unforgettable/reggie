package com.itheima.vo;

import com.itheima.domain.Employee;
import lombok.Data;

/**
 * @author CSY
 * 2022/5/9    16:06
 */
@Data
public class EmpTokenVo extends Employee {
    private String token;
}
