package com.itheima.vo;

import com.itheima.dto.OrdersDto;
import lombok.Data;

import java.util.List;

/**
 * @author CSY
 * 2022/5/14    11:48
 */
@Data
public class UserOrdersVo {

    private List<OrdersDto> records;

    private Integer total;

    private Integer size;

    private Integer current;

    private boolean optimizeCountSql;

    private boolean hitCount;

    private String countId;

    private String maxLimit;

    private boolean searchCount;

    private Integer pages;


}
