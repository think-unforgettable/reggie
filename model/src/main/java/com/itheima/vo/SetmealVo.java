package com.itheima.vo;

import com.itheima.dto.DishDto;
import com.itheima.dto.SetmealDto;
import lombok.Data;

import java.util.List;

/**
 * @author CSY
 * 2022/5/9    21:08
 */
@Data
public class SetmealVo {

    //分页vo
    private List<SetmealDto> records;
    private Integer total;
    private Integer current;
    private Integer size;

}
