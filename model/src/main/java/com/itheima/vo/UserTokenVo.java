package com.itheima.vo;

import com.itheima.domain.User;
import lombok.Data;

/**
 * @author CSY
 * 2022/5/12    13:51
 */
@Data
public class UserTokenVo extends User {

    private String token;
}
