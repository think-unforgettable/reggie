package com.itheima.constant;

/**
 * @author csy
 * @since 2022-04-12
 */
public class TokenConstant {
    public final static String EMPLOYEE_TOKEN_PREFIX = "employee_token_";
    public final static String USER_TOKEN_PREFIX = "user_token_";
}
